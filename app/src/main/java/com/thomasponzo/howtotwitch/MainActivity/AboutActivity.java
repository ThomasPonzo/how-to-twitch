package com.thomasponzo.howtotwitch.MainActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thomasponzo.howtotwitch.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;


/**
 * Created by Dell on 24-1-2018.
 */
@EActivity(R.layout.about_activity)
public class AboutActivity extends AppCompatActivity {
    private Toolbar toolbar;


    @AfterViews
    void onViewsInitialized() {




        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ImageView actionbarimage = findViewById(R.id.actionbar_logo);
        actionbarimage.setImageResource(R.drawable.ic_arrow_back_white_24dp);

        //Set the titel of the actionbar
        TextView actionbartitel =  findViewById(R.id.actionbar_titel);
        actionbartitel.setText(getResources().getString(R.string.about_toolbar_tittle));





        actionbarimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Todo check how to go to previous activitiy
                Intent mainIntent = new Intent(AboutActivity.this, MainActivity_.class);

                // Start the new activity
                startActivity(mainIntent);
            }
        });

    }
}
