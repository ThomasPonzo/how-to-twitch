package com.thomasponzo.howtotwitch.MainActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.mopub.mobileads.MoPubView;
import com.thomasponzo.howtotwitch.R;
import com.thomasponzo.howtotwitch.adapters.MainmenuAdapter;
import com.thomasponzo.howtotwitch.getters.setters.MainmenuGS;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;


@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.mainlistlayout)
    ListView mMainlistview;

    @ViewById(R.id.adview)
    MoPubView mMoPubView;

    @ViewById(R.id.app_bar)
    Toolbar toolbar;

    @ViewById(R.id.actionbar_logo)
    ImageView mAactionbarImage;

    @ViewById(R.id.actionbar_titel)
    TextView mActionbarTitel;


    @AfterViews
    void onViewsInitialized() {


        String adunitid = "b195f8dded45fe847ad89ed1d016da";

        setSupportActionBar(toolbar);

        mAactionbarImage.setImageResource(R.mipmap.ic_launcher);

        //Set the titel of the actionbar
        mActionbarTitel.setText("How To Twitch");


//        moPubView.setAdUnitId("b9175af8e4924f1f97d05dbcd8090319"); // Enter your Ad Unit ID from www.mopub.com
        mMoPubView.setTesting(true);
        mMoPubView.loadAd();

        loadMoPubView(mMoPubView, adunitid);


        ArrayList<MainmenuGS> mMainlist = new ArrayList<>();


        mMainlist.add(new MainmenuGS(getResources().getString(R.string.mainactivity_obsclassic), (getResources().getString(R.string.mainactivity_obsclassic_time)), (getResources().getString(R.string.mainactivity_obsclassic_status)), "2",
                Typeface.createFromAsset(getAssets(), "fonts/obsclassicFont.ttf"), Typeface.createFromAsset(getAssets(), "fonts/timeFont.ttf"),
                getResources().getInteger(R.integer.mainactivity_obsclassictextsize), getResources().getInteger(R.integer.mainactivity_obsclassic_timetextsize), getResources().getInteger(R.integer.mainactivity_obsclassic_statustextsize),
                getResources().getColor(R.color.main_obsclassic), R.drawable.obslogo, R.drawable.windowslogo, ObsclassicActivity_.class));

        mMainlist.add(new MainmenuGS(getResources().getString(R.string.mainactivity_obsstudio), (getResources().getString(R.string.mainactivity_obsstudio_time)), (getResources().getString(R.string.mainactivity_obsstudio_status)), "0",
                Typeface.createFromAsset(getAssets(), "fonts/obsstudioFont.ttf"), Typeface.createFromAsset(getAssets(), "fonts/timeFont.ttf"),
                getResources().getInteger(R.integer.mainactivity_obsstudiotextsize), getResources().getInteger(R.integer.mainactivity_obsstudio_timetextsize), getResources().getInteger(R.integer.mainactivity_obsstudio_statustextsize),
                getResources().getColor(R.color.main_obsstudio), R.drawable.obslogo, R.drawable.windowslogo, NotAvailable_.class));

        mMainlist.add(new MainmenuGS(getResources().getString(R.string.mainactivity_xsplit), (getResources().getString(R.string.mainactivity_xsplit_time)), (getResources().getString(R.string.mainactivity_xsplit_status)), "0",
                Typeface.createFromAsset(getAssets(), "fonts/xsplitFont.ttf"), Typeface.createFromAsset(getAssets(), "fonts/timeFont.ttf"),
                getResources().getInteger(R.integer.mainactivity_xsplittextsize), getResources().getInteger(R.integer.mainactivity_xsplit_timetextsize), getResources().getInteger(R.integer.mainactivity_xsplit_statustextsize),
                getResources().getColor(R.color.main_xsplit), R.drawable.xsplitlogo, R.drawable.windowslogo, NotAvailable_.class));
        mMainlist.add(new MainmenuGS(getResources().getString(R.string.mainactivity_elgato), (getResources().getString(R.string.mainactivity_elgato_time)), (getResources().getString(R.string.mainactivity_elgato_status)), "0",
                Typeface.createFromAsset(getAssets(), "fonts/elgatoFont.ttf"), Typeface.createFromAsset(getAssets(), "fonts/timeFont.ttf"),
                getResources().getInteger(R.integer.mainactivity_xsplittextsize), getResources().getInteger(R.integer.mainactivity_xsplit_timetextsize), getResources().getInteger(R.integer.mainactivity_xsplit_statustextsize),
                getResources().getColor(R.color.main_xsplit), R.drawable.xsplitlogo, R.drawable.windowslogo, NotAvailable_.class));

        final MainmenuAdapter mAdapter = new MainmenuAdapter(this, mMainlist);
        mMainlistview.setAdapter(mAdapter);

        // Set a click listener to play the audio when the list item is clicked on
        mMainlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent mainIntent = new Intent(MainActivity.this, mAdapter.getItem(position).cls);

                // Start the new activity
                startActivity(mainIntent);
            }
        });
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_rate:
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
            case R.id.menu_about:
                Intent aboutIntent = new Intent(MainActivity.this, AboutActivity_.class);
                startActivity(aboutIntent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public void onBackPressed() {

    }

    public void loadMoPubView(MoPubView moPubView, String adUnitId) {
        moPubView.setAdUnitId(adUnitId);
        moPubView.setAutorefreshEnabled(true);
        moPubView.loadAd();

    }
}
