package com.thomasponzo.howtotwitch.MainActivity;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.thomasponzo.howtotwitch.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


/**
 * Created by Dell on 8-12-2017.
 */
@EActivity(R.layout.currently_not_available)
public class NotAvailable extends AppCompatActivity {

    private Toolbar toolbar;

    @ViewById(R.id.available_description_start)
    TextView description;

    @AfterViews
    void onViewsInitialized() {


        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ImageView actionbarimage = findViewById(R.id.actionbar_logo);
        actionbarimage.setImageResource(R.drawable.ic_arrow_back_white_24dp);

        //Set the titel of the actionbar
        TextView actionbartitel =  findViewById(R.id.actionbar_titel);
        actionbartitel.setText(getResources().getString(R.string.not_available_title));

        description.setText(R.string.not_avaible_description);

        FrameLayout imageToolBar = findViewById(R.id.actionbar_logo_click);
        imageToolBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(NotAvailable.this, MainActivity_.class);

                // Start the new activity
                startActivity(mainIntent);
            }
        });


        actionbarimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.about_menu, menu);
        return super.onCreateOptionsMenu(menu);

    }
}
