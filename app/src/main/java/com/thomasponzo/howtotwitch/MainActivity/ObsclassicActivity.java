package com.thomasponzo.howtotwitch.MainActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.thomasponzo.howtotwitch.R;
import com.thomasponzo.howtotwitch.adapters.LessonsAdapter;
import com.thomasponzo.howtotwitch.getters.setters.Subjects;
import com.thomasponzo.howtotwitch.videoPlayer.VideoPlayer;
import com.thomasponzo.howtotwitch.videoPlayer.VideoPlayer_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;


/**
 * Created by Dell on 24-5-2017.
 */
@EActivity(R.layout.obsclassic_main)
public class ObsclassicActivity extends AppCompatActivity {

    @ViewById(R.id.actionbar_titel)
    TextView actionbartitel;

    @ViewById(R.id.actionbar_logo)
    ImageView actionbarimage;

    @ViewById(R.id.list)
    ListView listView;

    @ViewById(R.id.app_bar)
    Toolbar toolBar;



    @AfterViews
    void onViewsInitialized() {

        setSupportActionBar(toolBar);

        Typeface obsclassicfont = Typeface.createFromAsset(getAssets(), "fonts/obsclassicFont.ttf");
        actionbartitel.setTextSize(30);
        actionbartitel.setTypeface(obsclassicfont);
        actionbartitel.setText("Obs Classic");

        actionbarimage.setImageResource(R.drawable.ic_arrow_back_white_24dp);
        // Set a click listener on that View
        actionbarimage.setOnClickListener(new View.OnClickListener() {
            // The code in this method will be executed when the family category is clicked on.
            @Override
            public void onClick(View view) {
                // Create a new intent to open the {@link FamilyActivity}
                Intent mainIntent = new Intent(ObsclassicActivity.this, MainActivity.class);

                // Start the new activity
                startActivity(mainIntent);
                finish();
            }
        });

        ArrayList<Subjects> obsclassiclist = new ArrayList<Subjects>();

        obsclassiclist.add(new Subjects(getResources().getString(R.string.cl01_tittle), (getResources().getString(R.string.cl01_description)), (getResources().getString(R.string.cl01_time)),
                getResources().getColor(R.color.list_cl_lightblue),
                        Typeface.createFromAsset(getAssets(), "fonts/obsclassicFont.ttf"), Typeface.createFromAsset(getAssets(),
                "fonts/timeFont.ttf"), VideoPlayer_.class));

        obsclassiclist.add(new Subjects((getResources().getString(R.string.cl02_tittle)), (getResources().getString(R.string.cl02_description)), (getResources().getString(R.string.cl02_time)),
                getResources().getColor(R.color.list_cl_darkblue),
                Typeface.createFromAsset(getAssets(), "fonts/obsclassicFont.ttf"), Typeface.createFromAsset(getAssets(), "fonts/timeFont.ttf"), MainActivity_.class));

        final LessonsAdapter adapter = new LessonsAdapter(this, obsclassiclist);
        listView.setAdapter(adapter);

        // Set a click listener to play the audio when the list item is clicked on
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent mainIntent = new Intent(ObsclassicActivity.this, adapter.getItem(position).cls);

                // Start the new activity
                startActivity(mainIntent);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_rate:
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                }
                catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
            case R.id.menu_about:
                Intent aboutIntent = new Intent(ObsclassicActivity.this, AboutActivity.class);
                startActivity(aboutIntent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.getItem(0).setIcon(R.drawable.ic_more_vert_white_24dp);
        return super.onCreateOptionsMenu(menu);

    }
}