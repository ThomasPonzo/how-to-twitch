package com.thomasponzo.howtotwitch;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.thomasponzo.howtotwitch.MainActivity.MainActivity;
import com.thomasponzo.howtotwitch.MainActivity.MainActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;



@EActivity(R.layout.activity_welcome)
public class WelcomeActivity extends AppCompatActivity {

    @ViewById(R.id.welcome_screen_title)
    TextView title;

    @ViewById(R.id.welcome_screen_state)
    TextView stateVersion;

    @ViewById(R.id.welcome_screen_version)
    TextView version;

    @ViewById(R.id.welcome_screen_description)
    TextView description;

    @ViewById(R.id.welcome_screen_description_version)
    TextView versiondescription;

    @ViewById(R.id.welcome_screen_logo)
    ImageView logo;


    @AfterViews
    void onViewsInitialized() {


        String versionCode = BuildConfig.VERSION_NAME;

        Typeface obsclassicfont = Typeface.createFromAsset(getAssets(), "fonts/obsclassicFont.ttf");

        title.setText(getResources().getString(R.string.welcomeactivity_tittle));
        title.setTextSize(40);
        title.setTypeface(obsclassicfont);
        title.setTextColor(this.getResources().getColor(R.color.white));

        stateVersion.setText(getResources().getText(R.string.welcomeactivity_state));
        stateVersion.setTextSize(20);
        stateVersion.setTypeface(obsclassicfont);
        stateVersion.setTextColor(this.getResources().getColor(R.color.white));

        version.setText(versionCode);
        version.setTextSize(20);
        version.setTypeface(obsclassicfont);
        version.setTextColor(this.getResources().getColor(R.color.white));

        description.setText(getResources().getText(R.string.welcomeactivity_desciption));
        description.setTextSize(25);
        description.setTypeface(obsclassicfont);
        description.setVisibility(View.INVISIBLE);
        description.setTextColor(this.getResources().getColor(R.color.white));

        versiondescription.setText(getResources().getText(R.string.welcomeactivity_version));
        versiondescription.setTextSize(20);
        versiondescription.setTypeface(obsclassicfont);
        versiondescription.setTextColor(this.getResources().getColor(R.color.white));

        logo.setVisibility(View.INVISIBLE);
        logo.setImageResource(R.mipmap.ic_launcher);

        Animation animationtitle = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.welcome_animation_title);
        Animation animationstate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.welcome_animation_public);
        Animation animationdescriptionversion = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.welcome_animation_description_version);
        final Animation animationlogo = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.welcome_animation_logo);
        final Animation animationdescription = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.welcome_animation_description);

        title.setAnimation(animationtitle);
        versiondescription.setAnimation(animationdescriptionversion);
        stateVersion.setAnimation(animationstate);
        version.setAnimation(animationtitle);

        animationtitle.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                logo.setAnimation(animationlogo);
                logo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationlogo.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                description.setAnimation(animationdescription);
                description.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationdescription.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

// Create a new intent to open the {@link FamilyActivity}
                Intent mainscreen = new Intent(WelcomeActivity.this, MainActivity_.class);

                // Start the new activity
                startActivity(mainscreen);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
