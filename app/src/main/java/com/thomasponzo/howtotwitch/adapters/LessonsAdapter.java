package com.thomasponzo.howtotwitch.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thomasponzo.howtotwitch.R;
import com.thomasponzo.howtotwitch.getters.setters.Subjects;

import java.util.ArrayList;

/**
 * Created by Dell on 10-10-2017.
 */


    public class LessonsAdapter extends ArrayAdapter<Subjects> {
    public LessonsAdapter(Activity context, ArrayList<Subjects> mainmenu) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, mainmenu);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        Subjects Subjects = getItem(position);

        TextView Subject = listItemView.findViewById(R.id.subject);
        Subject.setText(Subjects.getObsclassicSubject());

        TextView Description = listItemView.findViewById(R.id.description);
        Description.setText(Subjects.getObsclassicDescription());

        TextView Time = listItemView.findViewById(R.id.time);
        Time.setText(Subjects.getObsclassictime());

        Subject.setTypeface(Subjects.getObsclassicfontid());
        Description.setTypeface(Subjects.getObsclassicfontid());

        Time.setTypeface(Subjects.getObsclassictimefontid());

        LinearLayout linearLayout = listItemView.findViewById(R.id.mainlistview);
        linearLayout.setBackgroundColor(Subjects.getMainBackgroundColor());

        return listItemView;
    }
}
