package com.thomasponzo.howtotwitch.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.thomasponzo.howtotwitch.R;
import com.thomasponzo.howtotwitch.getters.setters.MainmenuGS;

import java.util.ArrayList;

/**
 * Created by Dell on 29-9-2017.
 */



public class MainmenuAdapter extends ArrayAdapter<MainmenuGS> {
    public MainmenuAdapter(Activity context, ArrayList<MainmenuGS> mainmenu) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, mainmenu);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_menu, parent, false);
        }


        MainmenuGS mainSubjects = getItem(position);

        TextView mainTittleTextView = listItemView.findViewById(R.id.menu_list_tittle);
        mainTittleTextView.setText(mainSubjects.getMainTittle());

        TextView mainTimeTextView = listItemView.findViewById(R.id.menu_list_time);
        mainTimeTextView.setText(mainSubjects.getMainTime());

        TextView mainStatusTextView = listItemView.findViewById(R.id.menu_list_status);
        mainStatusTextView.setText(mainSubjects.getMainStatus());

        TextView mainItemsTextView = listItemView.findViewById(R.id.menu_list_item);
        mainItemsTextView.setText(mainSubjects.getMainitems());

        mainTittleTextView.setTypeface(mainSubjects.getMainTextFont());
        mainStatusTextView.setTypeface(mainSubjects.getMainTextFont());

        mainTimeTextView.setTypeface(mainSubjects.getMainTimeFont());

        mainTittleTextView.setTextSize(mainSubjects.getMainTittleTextsize());
        mainTimeTextView.setTextSize(mainSubjects.getMainTimeTextsize());
        mainStatusTextView.setTextSize(mainSubjects.getMainStatusTextsize());

        RelativeLayout linearLayout = listItemView.findViewById(R.id.mainmenu_layoutlist);
        linearLayout.setBackgroundColor(mainSubjects.getMainBackgroundColor());

        ImageView program = listItemView.findViewById(R.id.menu_list_imageprogram);
        program.setImageResource(mainSubjects.getMainProgramImage());

        ImageView system = listItemView.findViewById(R.id.menu_list_imagesystem);
        system.setImageResource(mainSubjects.getMainSystemImage());

        return listItemView;
    }
}
