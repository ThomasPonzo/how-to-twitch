package com.thomasponzo.howtotwitch.getters.setters;

/**
 * Created by Dell on 29-9-2017.
 */

public class MainmenuGS {
    private String MainTittle;

    private String MainTime;

    private String MainStatus;

    private String Mainitems;

    private android.graphics.Typeface MainTextFont;

    private android.graphics.Typeface MainTimeFont;

    private int MainTittleTextsize;

    private int MainTimeTextsize;

    private int MainStatusTextsize;

    private int MainBackgroundColor;

    private int MainProgramImage;

    private int MainSystemImage;

    public Class cls;


    public MainmenuGS (String maintittlelist, String mainTime, String mainStatus, String items,
                       android.graphics.Typeface mainTextFont, android.graphics.Typeface mainTimeFont,
                       int mainTittleTextsize, int mainTimeTextsize, int mainStatusTextsize,
                       int mainBackgroundColor, int mainProgramImage, int mainSystemImage, Class mainclass) {
        MainTittle = maintittlelist;

        MainTime = mainTime;

        MainStatus = mainStatus;

        Mainitems = items;

        MainTextFont = mainTextFont;

        MainTimeFont = mainTimeFont;

        MainTittleTextsize = mainTittleTextsize;

        MainTimeTextsize = mainTimeTextsize;

        MainStatusTextsize = mainStatusTextsize;

        MainBackgroundColor = mainBackgroundColor;


        MainProgramImage = mainProgramImage;

        MainSystemImage = mainSystemImage;

        cls = mainclass;
    }

    public String getMainTittle() {return  MainTittle;}

    public String getMainTime() {return  MainTime;}

    public String getMainStatus() {return  MainStatus;}

    public String getMainitems() {return Mainitems;}

    public android.graphics.Typeface getMainTextFont(){return MainTextFont;}

    public android.graphics.Typeface getMainTimeFont(){return MainTimeFont;}

    public int getMainTittleTextsize(){return MainTittleTextsize;}

    public int getMainTimeTextsize(){return  MainTimeTextsize;}

    public int getMainStatusTextsize(){return  MainStatusTextsize;}

    public int getMainBackgroundColor(){return  MainBackgroundColor;}

    public int getMainProgramImage(){return MainProgramImage;}

    public int getMainSystemImage(){return  MainSystemImage;}




}
