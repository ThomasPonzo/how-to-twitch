package com.thomasponzo.howtotwitch.getters.setters;


import android.graphics.Color;

/**
 * {@link Subjects} represents a subject  that the user wants to learn.
 * It contains a subject, description of the subject, and an image for that subject.
 */
public class Subjects {

    /**The subject */
    private String obsclassicSubject;

    /** The description of the subject */
    private String obsclassicDescription;

    private String obsclassictime;

    private int MainBackgroundColor;

    private android.graphics.Typeface obsclassicfontid;

    private android.graphics.Typeface obsclassictimefontid;

    public Class cls;

    /**
     * Create a new Subjects object.
     *
     * @param subject is the subject on obs classic
     * @param description is the description of the subject
     */
    public Subjects(String subject , String description, String time,
                    int mainBackgroundColor, android.graphics.Typeface font,
                    android.graphics.Typeface fonttime , Class intent) {
        obsclassicSubject = subject;
        obsclassicDescription = description;

        obsclassictime = time;
        MainBackgroundColor = mainBackgroundColor;
        obsclassicfontid = font;
        obsclassictimefontid = fonttime;
        cls = intent;
    }

    /**
     * Get the subject.
     */
    public String getObsclassicSubject() {return  obsclassicSubject;}

    /**
     * Get the description of the subject.
     */
    public String getObsclassicDescription() {return obsclassicDescription;}

    /**
     * Get the imageid
     */


    /**
     * Get the imageid
     */
    public String getObsclassictime() {return obsclassictime;}

    public int getMainBackgroundColor(){return  MainBackgroundColor;}

    public android.graphics.Typeface getObsclassicfontid() {return obsclassicfontid;}

    public android.graphics.Typeface getObsclassictimefontid() {return  obsclassictimefontid;}

    public Class getCls() {return  cls;}
}


