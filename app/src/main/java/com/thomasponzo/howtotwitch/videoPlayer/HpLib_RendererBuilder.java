package com.thomasponzo.howtotwitch.videoPlayer;

/**
 * Created by kunal.bhatia on 05-05-2016.
 */
public interface HpLib_RendererBuilder {
    void buildRenderers(VideoPlayer player);
    void cancel();
}
